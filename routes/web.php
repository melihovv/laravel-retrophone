<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/logs', [
    'uses' => '\Melihovv\LaravelLogViewer\LaravelLogViewerController@index',
    'as' => 'logs',
]);

Route::get('/', 'HomeController@index')
    ->name('home.index');

Route::post('/feedback', 'FeedbackController@send')
    ->name('feedback.send');

Route::get('/shops', 'ShopsController@index')
    ->name('shops.index');

Route::get('/phones', 'PhonesController@index')
    ->name('phones.index');

Route::get('/phones/{phone}', 'PhonesController@show')
    ->name('phones.show');

Route::post('/phones/{phone}/order', 'PhonesController@order')
    ->name('phones.order');

Route::get('/search', 'SearchController@search')
    ->name('search.search');
