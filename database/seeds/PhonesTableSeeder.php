<?php

use App\Models\Phone;
use Illuminate\Database\Seeder;

class PhonesTableSeeder extends Seeder
{
    public function run()
    {
        factory(Phone::class, 10)->create();
    }
}
