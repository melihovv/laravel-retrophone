@extends('layouts.master')

@section('content')
    <p>Найденные результаты по запросу "{{ $query }}".</p>

    @forelse ($phones as $phone)
        <div class="row search-item">
            <div class="col-sm-4">
                <img src="{{ $phone->image }}" alt="{{ $phone->name }}"
                     class="search-item-image">
            </div>
            <div class="col-sm-4">{{ $phone->name }}</div>
            <div class="col-sm-4">
                <a href="{{ route('phones.show', $phone->id) }}"
                   class="btn btn-primary">
                    Показать
                </a>
            </div>
        </div>
    @empty
        <p>По вашему запросу не нашлось ни одного телефона.</p>
    @endforelse
@endsection
