@extends('layouts.master')

@section('content')
    @forelse ($shops as $shop)
        <div class="row shop">
            <div class="col-sm-3">
                <img src="{{ $shop->image }}" alt="{{ $shop->name }}"
                     class="shop-image">
            </div>
            <div class="col-sm-9">
                <h3>{{ $shop->name }}</h3>
                <p class="shop-desc">{{ $shop->desc }}</p>
            </div>
        </div>
    @empty
        Пока что нет ни одного магазина
    @endforelse
@endsection
