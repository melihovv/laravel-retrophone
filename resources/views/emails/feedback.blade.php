@component('mail::message')

Пользователь оставил следующее сообщение

@component('mail::panel')
    {{ $message }}
@endcomponent

@endcomponent
