@component('mail::message')

Пользователь заказал телефон "{{ $phone->name }}"

<h3>Детали заказа</h3>
<p>Имя: {{ $params['name'] }}</p>
<p>Фамилия: {{ $params['lastname'] }}</p>
<p>Отчество: {{ $params['surname'] }}</p>
<p>Телефон: {{ $params['phone'] }}</p>
<p>Email: {{ $params['email'] }}</p>
<p>Адрес доставки: {{ $params['addr'] }}</p>
<p>Дата доставки: {{ $params['date'] }}</p>
<p>Количество: {{ $params['amount'] }}</p>

@endcomponent
