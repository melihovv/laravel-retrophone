@extends('layouts.master')

@section('content')
    @forelse ($phones->chunk(3) as $chunk)
        <div class="row">
            @foreach ($chunk as $phone)
                <div class="col-sm-4 phone-card">
                    <div class="col-sm-3">
                        <img src="{{ $phone->image }}" alt="{{ $phone->name }}"
                             class="phone-card-image">
                    </div>
                    <div class="col-sm-9">
                        <h2>{{ $phone->name }}</h2>
                        <p class="phone-card-desc">{{ $phone->short_desc }}</p>
                        <a href="{{ route('phones.show', $phone->id) }}"
                           class="btn btn-primary">
                            Показать
                        </a>
                        <p class="phone-card-price">{{ $phone->price }} руб.</p>
                    </div>
                </div>
            @endforeach
        </div>
    @empty
        Пока что нет ни одного телефона
    @endforelse
@endsection
