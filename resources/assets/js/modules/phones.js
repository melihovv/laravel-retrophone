(function () {
  const price = $('#phone-modal-price').val()
  const $error = $('#phone-modal-error')
  const $info = $('#phone-modal-info')

  $('#phone-order').click(function (e) {
    const inputs = $('#phone-modal input')

    for (const input of inputs) {
      if (!input.checkValidity()) {
        $error.text('Заполните все поля').show()
        return
      }
    }
    $error.hide()

    const params = {
      name: $('#phone-modal-name').val(),
      lastname: $('#phone-modal-lastname').val(),
      surname: $('#phone-modal-surname').val(),
      phone: $('#phone-modal-phone').val(),
      email: $('#phone-modal-email').val(),
      addr: $('#phone-modal-addr').val(),
      date: $('#phone-modal-date').val(),
      amount: $('#phone-modal-amount').val(),
    };

    const components = location.pathname.split(/\//)
    const phoneId = components[components.length - 1]

    axios.post(`/phones/${phoneId}/order`, params)
      .then(function (response) {
        $error.hide()
        $info
          .text('Ваш заказ успешно принят')
          .show()
      })
      .catch(function (error) {
        $info.hide()
        $error.text('Не удалось отправить ваш заказ').show()
      })
  })

  $('#phone-modal-amount').change(function (e) {
    const amount = this.value

    $('#phone-modal-sum').text(amount * price)
  })
})()
