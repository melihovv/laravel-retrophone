(function () {
  $('#feedback-send').click(function (e) {
    const $error = $('#feedback-message-error')
    const $info = $('#feedback-message-info')
    const message = $('#feedback-message').val()

    if (message.trim() === '') {
      $error.text('Введите сообщение').show()
      return
    } else {
      $error.hide()
    }

    axios.post('/feedback', {message})
      .then(function (response) {
        $error.hide()
        $info
          .text('Ваше сообщение успешно отправлено')
          .show()
      })
      .catch(function (error) {
        $info.hide()
        $error.text('Не удалось отправить ваше сообщение').show()
      })
  })
})()
