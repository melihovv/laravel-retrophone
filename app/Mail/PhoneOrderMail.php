<?php

namespace App\Mail;

use App\Models\Phone;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PhoneOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Phone
     */
    public $phone;

    /**
     * @var array
     */
    public $params;

    /**
     * @param Phone $phone
     * @param array $params
     */
    public function __construct(Phone $phone, $params)
    {
        $this->phone = $phone;
        $this->params = $params;
    }

    public function build()
    {
        return $this
            ->markdown('emails.phone-order', [
                'phone' => $this->phone,
                'params' => $this->params,
            ])
            ->from(config('mail.admin_mail'))
            ->subject('Заказ телефона');
    }
}
