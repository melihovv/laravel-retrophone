<?php

namespace App\Http\Requests;

class SearchSearchRequest extends FormRequest
{
    public function rules()
    {
        return [
            'q' => 'nullable|string',
        ];
    }
}
