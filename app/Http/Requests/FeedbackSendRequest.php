<?php

namespace App\Http\Requests;

class FeedbackSendRequest extends FormRequest
{
    public function rules()
    {
        return [
            'message' => 'required|string',
        ];
    }
}
