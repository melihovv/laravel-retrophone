<?php

namespace App\Http\Requests;

class PhonesOrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'lastname' => 'required|string',
            'surname' => 'required|string',
            'addr' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|string',
            'amount' => 'required|string',
            'date' => 'required|string',
        ];
    }
}
