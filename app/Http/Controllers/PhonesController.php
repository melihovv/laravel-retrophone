<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhonesOrderRequest;
use App\Mail\PhoneOrderMail;
use App\Models\Phone;
use Illuminate\Support\Facades\Mail;

class PhonesController extends Controller
{
    public function index()
    {
        $phones = Phone::all();

        return view('phones.index', [
            'phones' => $phones,
        ]);
    }

    public function show(Phone $phone)
    {
        return view('phones.show', [
            'phone' => $phone,
        ]);
    }

    public function order(Phone $phone, PhonesOrderRequest $request)
    {
        $mail = new PhoneOrderMail($phone, $request->all());

        Mail::to(config('mail.admin_mail'))
            ->queue($mail);

        return response()->json(['message' => 'Ok']);
    }
}
