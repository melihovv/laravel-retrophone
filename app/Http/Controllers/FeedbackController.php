<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackSendRequest;
use App\Mail\FeedbackMail;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function send(FeedbackSendRequest $request)
    {
        $mail = new FeedbackMail($request->get('message'));

        Mail::to(config('mail.admin_mail'))
            ->queue($mail);

        return response()->json(['message' => 'Ok']);
    }
}
