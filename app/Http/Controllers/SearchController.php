<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchSearchRequest;
use App\Models\Phone;

class SearchController extends Controller
{
    public function search(SearchSearchRequest $request)
    {
        $query = $request->get('query');
        $phones = collect();

        if (!empty($query)) {
            $phones = Phone::where('name', 'LIKE', "%$query%")
                ->orWhere('desc', 'LIKE', "%$query%")
                ->get();
        }

        return view('search.search', [
            'phones' => $phones,
            'query' => $query,
        ]);
    }
}
