<?php

namespace App\Http\Controllers;

use App\Models\Shop;

class ShopsController extends Controller
{
    public function index()
    {
        $shops = Shop::all();

        return view('shops.index', [
            'shops' => $shops,
        ]);
    }
}
