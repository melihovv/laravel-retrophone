<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = [
        'name',
        'image',
        'desc',
        'short_desc',
        'price',
    ];
}
